/**
 *  Arlo Mode Control
 *
 *  Copyright 2019 Gareth Jeanne
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 *  Last Updated : 26/12/2019
 *
 */

private getApiUrl() { "https://my.arlo.com/" }

definition(
	name: "Arlo Mode Control",
	namespace: "gazzer82",
	author: "Gareth Jeanne",
	description: "Control the Arlo Hub mode from Hubitat",
	category: "Security",
	iconUrl:   "",
	iconX2Url: "",
	iconX3Url: ""
)

preferences {
	page(name: "prefLogIn", title: "Arlo Mode Control")
    page(name: "prefSelectHub", title: "Arlo Mode Control")
    page(name: "prefSelectHubModes", title: "Arlo Mode Control")
    page(name: "prefSelectArmSwitch", title: "Arlo Mode Control")
    page(name: "prefSelectHubitatHubModes", title: "Arlo Mode Control")
    page(name: "prefSelectHubitatHSMModes", title: "Arlo Mode Control")
}

/* Preferences */
def prefLogIn() {
	def showUninstall = username != null && password != null 
	return dynamicPage(name: "prefLogIn", title: "Connect to Arlo", nextPage:"prefSelectHub", uninstall:showUninstall, install: false) {
		section("Login Credentials"){
            paragraph "It is STONGLY reccomended to create a new user specificly for hubitat on Arlo, it allows you to remove access if credentials are compromised, and stop your main user getting logged out when Hubitat is active!"
			input("username", "text", title: "Username", description: "Arlo (case sensitive)", required: true)
			input("password", "password", title: "Password", description: "Arlo password (case sensitive)", required: true)
		}            
	}
}

def prefSelectHub() {
	LoginResult = loginCheck()
	if (LoginResult) 
	{
        state.devices = getDevices()
        def options = []
        state.devices.basestation.each { baseStation -> options << [ "${baseStation.uniqueId} ${baseStation.xCloudId} ${baseStation.deviceId}" : "${baseStation.deviceName}"]}
	    return dynamicPage(name: "prefSelectHub",  title: "Select Hub", uninstall:false, install: false, nextPage:"prefSelectHubModes") {
		    section("") { 
                paragraph "Please select which Arlo Hub you would like to use."
                input "hub", "enum", required: true, multiple: false, options: options, submitOnChange: true
            }
        }
	} else {
		return dynamicPage(name: "prefSelectHub",  title: "Error!", install:false) {
			section(""){ paragraph "The username or password you entered is incorrect. Click Done to try again. " }
		}  
	}
}

def prefSelectHubModes() {
    state.modes = getModes()
    def options = []
    state.modes.each { mode -> options << ["${mode.id}" : "${mode.id} - ${mode.type}"]}
    return dynamicPage(name: "prefSelectHubModes", title: "Select Modes", uninstall:false, install: false, nextPage: "prefSelectArmSwitch") {
        section("Armed Mode"){
            input "armedArloMode", "enum", required: true, multiple: false, options: options, submitOnChange: true
        }
        section("Disarmed Mode"){
            input "disarmedArloMode", "enum", required: true, multiple: false, options: options, submitOnChange: true
        }
    }
}

def prefSelectArmSwitch(){
    return dynamicPage(name: "prefSelectHub",  title: "Select Hub", install:false, nextPage: "prefSelectHubitatHubModes") {
        section("") { 
            paragraph "Please select which Arlo Hub you would like to use."
                input "armSwitch", "capability.switch", required: false
            }
        }
}

def prefSelectHubitatHubModes(){
    def options = []
    location.modes.each { mode -> options << ["$mode": "$mode"]}
    log.debug options
    return dynamicPage(name: "prefSelectHubitatHubModes", title: "Select Hub Modes", install: false, nextPage: "prefSelectHubitatHSMModes") {
        section("") {
            paragraph "All other modes will Disarm Arlo."
            paragraph "Please select which Hub Modes you would like to Arm Arlo, if you do not wish to use Hubitat modes to contol Arlo then do not select any options below."
            paragraph "All other modes will Disarm Arlo."
        }
        section("Arm") {
            input "armedHubitatHubModes", "enum", required: false, multiple: true, options: options, submitOnChange: true
        }
    }
}

def prefSelectHubitatHSMModes(){
    def options = [
        "armedAway" : "Away", 
        "armedNight" : "Night",
        "armedHome" : "Home"
        ]
    return dynamicPage(name: "prefSelectHubitatHSMModes", title: "Select HSM Modes", install: true) {
         section("") {
            paragraph "Please select which HSM Armed Modes you would like to Arm Arlo, if you do not wish to use Hubitat modes to contol Arlo then do not select any options below."
            paragraph "All other modes will Disarm Arlo."
        }
        section("Arm") {
            input "armedHubitatHSMModes", "enum", required: false, multiple: true, options: options, submitOnChange: true
        }
    }
}

/* Initialization */
def installed() {
    unsubscribe()
    unschedule()
    initialize() 
}

def updated() { 
	unsubscribe()
    unschedule()
	initialize() 
}

def uninstalled() {
	unschedule()
	getAllChildDevices().each { deleteChildDevice(it.deviceNetworkId) }
}	

def initialize() {
    log.debug "initialize() with settings: ${settings}"
    // If not existing token, go and fetch one
    if(!state.token){
        login()
    }
    // Set a timer to renew the token every hour
    runEvery1Hour("login")
    // Subscribe to switches if they exist
    if(armSwitch){
        log.debug "We have an arm switch so subscribing"
        subscribe(armSwitch, "switch", switchHandler)
    } else {
        log.debug "No arm switch so not subscribing"
    }
    //Subscribe to Hubitat Hub modes if selected
    if(armedHubitatHubModes){
        log.debug "Hubitat Hub Modes selected so subscribing to mode changes"
        subscribe(location, "mode", hubitatHubModeChangeHandler)
    } else {
        log.debug "No Hubitat Hub Modes selected so not subscribing"
    }
    //Subscribe to Hubitat HSM modes if selected
    if(armedHubitatHSMModes){
        log.debug "Hubitat HSM Modes selected so subscribing to changes"
        subscribe(location, "hsmStatus", hubitatHSMModeChangeHandler)
    } else {
        log.debug "No Hubitat HSM Modes selected so not subscribing"
    }
}

def switchHandler(evt){
    log.debug "Arlo Switch Handler ${evt.value}"
    if(evt.value == "on"){
        armArlo()
    } else {
        disarmArlo()
    }
}

def hubitatHubModeChangeHandler(evt){
    log.debug "Hubitat Hub mode changed to ${evt.value}"
    if(armedHubitatHubModes.contains(evt.value)){
        log.debug("Mode $evt.value contained is Hub Modes list so arming Arlo")
        armArlo()
    } else {
        log.debug("Mode $evt.value not contained is Hub Modes list so disarming Arlo")
        disarmArlo()
    }
}

def hubitatHSMModeChangeHandler(evt){
    log.debug "Hubitat HSM mode changed to ${evt.value}"
    if(armedHubitatHSMModes.contains(evt.value)){
        log.debug("Mode $evt.value contained is HSM Modes list so arming Arlo")
        armArlo()
    } else {
        //Check if it's an Arming mode in which case ignore it
        if(evt.value.contains("arming")){
            log.debug("Arming based HSM mode change, so ignoring it");
        } else {
            log.debug("Mode $evt.value not contained is HSM Modes list so disarming Arlo")
            disarmArlo()
        }
    }
}

private login() {
    def request = [
        uri: apiUrl,
        path: "hmsweb/login/v2",
        requestContentType: "application/json",
        contentType: "application/json",
        body: [
            email: username,
            password: password
        ]
    ]
    try {
        httpPost(request) { resp ->
        log.debug "response data: ${resp.data}"
            if(resp.data.success){
                state.token = resp.data.data.token
            }
        }
    } catch (e) {
        log.error(e)
    }
}

private getDevices(){
    def devices = [:];
    def request = [
        uri: apiUrl,
        path: "hmsweb/users/devices",
        requestContentType: "application/json",
        contentType: "application/json",
        headers: [
            Authorization: state.token
        ]
    ]
    try {
        httpGet(request) { resp ->
            if(resp.data.success){
                hubDetails = resp.data.data.each{
                    device ->
                    if(!devices[device.deviceType]){
                      devices[device.deviceType] = []
                    }
                    devices[device.deviceType] << device
                }
            }
        }
    } catch (e) {
        log.error(e)
    }
	return devices
}

private getModes(){
    def modes = [:];
    def hubValues = getHubValues()
    def request = [
        uri: apiUrl,
        path: "hmsweb/users/automation/definitions",
        requestContentType: "application/json",
        query: [
            uniqueIds: "all"
        ],
        contentType: "application/json",
        headers: [
            Authorization: state.token
        ]
    ]
    try {
        httpGet(request) { resp ->
            if(resp.data.success){
                log.debug hubValues.uniqueId
                log.debug resp.data.data
                modes = resp.data.data["${hubValues.uniqueId}"].modes
            }
        }
    } catch (e) {
        log.error(e)
    }
    log.debug modes
	return modes
}

// Split the hub info stored in the string from setup
private getHubValues(){
    def hubValues = hub.split()
    return [
        uniqueId: hubValues[0],
        xCloudId: hubValues[1],
        deviceId: hubValues[2]
    ]
}

// Verify valid login credentials
private loginCheck() {
    // Try the credntials
	login()
    return state.token
}

private setArloMode(mode) {
    // def hubDetails = getHubDetails()
    def hubValues = getHubValues()
    def now = new Date();
    if(state.token && hubValues){
        def request = [
            uri: apiUrl,
            path: "hmsweb/users/devices/notify/${hubValues.deviceId}",
            requestContentType: "application/json",
            contentType: "application/json",
            headers: [
                Authorization: state.token,
                xcloudid: hubValues.xCloudId,
                "Content-Type": "application/json; charset=utf-8",
                "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.3.5 (KHTML, like Gecko) Mobile/15B202 NETGEAR/v1 (iOS Vuezone)"
            ],
            body: [
                from :"gazzer82-hubitat",
	            to:hubValues.deviceId,
	            action:"set",
	            resource:"modes",
                transId:"web!XXXXXXXX.XXXXXXXXXXXXXXX XXXXX",
	            publishResponse:true,
	            properties:
		            [
			            active: mode
		            ]
            ]
        ]
        log.debug request
        try {
            httpPost(request) { resp ->
            log.debug "response data: ${resp.data}"
                if(resp.data.success){
                    log.debug("Arlo set to mode: ${mode}")
                }
            }
        } catch (e) {
            log.error("Failed to set Arlo mode");
            log.error(e)
        }
    } else {
        log.error("No token or hub during mode set")
    }
}

private armArlo() {
    setArloMode(armedArloMode)
}

private disarmArlo() {
    setArloMode(disarmedArloMode)
}

